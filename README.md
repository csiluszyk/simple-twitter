simple-twitter
==============

Installation
------------

Ensure that required system dependencies are installed:

* python2.7
* [virtualenv](http://www.virtualenv.org/en/latest/index.html)
* git

It should be easier to begin with a separate folder at first:

    mkdir simple-twitter
    cd simple-twitter

and to install script inside a virtualenv

    virtualenv --python=python2.7 venv
    . venv/bin/activate

Then script and its dependencies can be installed by simply running:

    git clone https://csiluszyk@bitbucket.org/csiluszyk/simple-twitter.git
    cd simple-twitter
    pip install -e .

If installation exits with *ImportError: No module named django* error try
install Django first, i.e.:

    pip install django

and try again:

    pip install -e .

After this you can run twitter:

    python manage.py runserver 0.0.0.0:8000
