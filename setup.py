#!/usr/bin/python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

import os
import sys

if os.getuid() == 0:  # root
    print >>sys.stderr, "ERROR: This setup.py cannot be run as root."
    print >>sys.stderr, "ERROR: If you want to proceed anyway, hunt this"
    print >>sys.stderr, "ERROR: message and edit the source at your own risk."
    sys.exit(2)

setup(
    name='simple_twitter',
    version = '0.1.dev',
    description='Simple twitter.',
    author='Cezary Siłuszyk',
    author_email='cezarysiluszyk@gmail.com',
    url='https://bitbucket.org/csiluszyk/simple-twitter',
    install_requires=[
        "Django>=1.5",
        "django-registration>=0.9b1",
        "django-compressor",
    ],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    dependency_links=[
        'http://bitbucket.org/mszamot/django-registration/get/default.zip#egg=django-registration-0.9b1',
    ],
)
