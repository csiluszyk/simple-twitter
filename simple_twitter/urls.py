from django.conf.urls import patterns, url

from twitt.views import Index, Registration, Thanks, UserTwitts

urlpatterns = patterns('',
    url(r'^$', Index.as_view(), name='index'),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^register/$', Registration.as_view(), name='register'),
    url(r'^thanks/$', Thanks.as_view(), name='thanks'),
    url(r'^(?P<author>\w+)/$', UserTwitts.as_view(), name='user_twitts'),
)
