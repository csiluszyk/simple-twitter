from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from django.utils import timezone
from django.views.generic import TemplateView, FormView, View
from django.contrib.auth.forms import UserCreationForm
from twitt.forms import TwittForm
from twitt.models import Twitt


class Index(View):
    def get(self, request, *args, **kwargs):
        if request.user.id:
            form = TwittForm(request.POST or None)
            twitts = Twitt.objects.all().order_by('-pub_date')[:10]
            return TemplateResponse(request, 'index.html',
                                    context={'twitts': twitts, 'form': form})
        else:
            return TemplateResponse(request, 'anon.html')

    def post(self, request, *args, **kwargs):
        form = TwittForm(request, request.POST)
        twitts = Twitt.objects.all().order_by('-pub_date')[:10]
        instance = form.save(commit=False)
        instance.author = request.user
        instance.pub_date = timezone.now()
        instance.save()
        return TemplateResponse(request, 'index.html',
                                context={'twitts': twitts,
                                         'form': TwittForm(request)})


class Registration(FormView):
    template_name = 'registration/register.html'
    form_class = UserCreationForm
    success_url = '/thanks/'

    def form_valid(self, form):
        form.save()
        return super(Registration, self).form_valid(form)


class Thanks(TemplateView):
    template_name = 'thanks.html'


class UserTwitts(View):
    def get(self, request, *args, **kwargs):
        author = User.objects.get(username=kwargs['author'])
        twitts = Twitt.objects.filter(author=author).order_by('-pub_date')
        return TemplateResponse(request, 'user_twitts.html',
                                context={'twitts': twitts})
