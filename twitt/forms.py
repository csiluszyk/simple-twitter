from django.forms import ModelForm
from twitt.models import Twitt


class TwittForm(ModelForm):
    class Meta:
        model = Twitt
        fields = ['text']

    def __init__(self, request, *args, **kwargs):
        super(TwittForm, self).__init__(*args, **kwargs)
        self.fields['text'].widget.attrs['class'] = 'input-xxlarge'
